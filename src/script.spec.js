/**
 * @overview  script.spec
 */

import $ from 'jquery';
import jQueryMock from './jquery.mock';

jest.mock('jquery');


describe('Main script', function() {
  let init,
      onSearchFormSubmit,
      filterProducts;

  const PRODUCTS = [
    'Latitude',
    'Longitude',
    'Arctic Circle',
    'Antarctic Circle'
  ];

  beforeAll(() => {
    $.mockReturnValue(jQueryMock);

    const scriptModule = require('./script');
    init               = scriptModule.init;
    onSearchFormSubmit = scriptModule.onSearchFormSubmit;
    filterProducts     = scriptModule.filterProducts;
  });

  it('init() starts listening the form submission', () => {
    expect(jQueryMock.ready).toBeCalled();
    expect(jQueryMock.submit).toBeCalled();
  });

  describe('onSearchFormSubmit', () => {
    const eventMock = { preventDefault: jest.fn() };

    afterAll(() => {
      $.mockReturnValue(jQueryMock);
    });

    it('prevents the native form submission', () => {
      onSearchFormSubmit(eventMock);

      expect(eventMock.preventDefault).toBeCalled();
    });

    it('filters the products', () => {
      const attrSpy = jest.fn();
      PRODUCTS.forEach(p => attrSpy.mockReturnValueOnce(p));

      $.mockReturnValue({
        ...jQueryMock,
        ...{
          val: () => 'TUDE',
          attr: attrSpy,
          each: cb => PRODUCTS.forEach(cb)
        }
      });

      onSearchFormSubmit(eventMock);

      expect(jQueryMock.removeClass).toBeCalledTimes(2);
      expect(jQueryMock.removeClass.mock.calls[0][0]).toBe('d-none');

      expect(jQueryMock.addClass).toBeCalledTimes(2);
      expect(jQueryMock.addClass.mock.calls[0][0]).toBe('d-none');
    });
  });
});