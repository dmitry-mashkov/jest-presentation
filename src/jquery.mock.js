/**
 * @overview  jquery.mock
 */

export default {
  ready: jest.fn().mockImplementation(cb => cb()),
  submit: jest.fn(),
  val: jest.fn(),
  each: jest.fn(),
  removeClass: jest.fn(),
  addClass: jest.fn()
};