/**
 * @overview  Main webpage script
 */

import $ from 'jquery';

const SEARCH_FORM_SELECTOR = '#search-form';
const SEARCH_INPUT_SELECTOR = SEARCH_FORM_SELECTOR + ' [type="search"]';
const PORTFOLIO_ITEM_SELECTOR = '.portfolio-item';


$(document).ready(init);


/**
 * Page initialization
 */
function init() {
  $(SEARCH_FORM_SELECTOR).submit(onSearchFormSubmit);
}

/**
 * Handles search form submission event
 *
 * @param {Event} e - native DOM event object
 */
function onSearchFormSubmit(e) {
  e.preventDefault();

  const $searchInputElement = $(SEARCH_INPUT_SELECTOR);

  filterProducts($searchInputElement.val());
}

/**
 * Filters the list of products by the passed search value
 *
 * @param {string} searchValue - search string
 */
function filterProducts(searchValue) {
  $(PORTFOLIO_ITEM_SELECTOR).each((i, portfolio) => {
    const name = $(portfolio).attr('data-portfolio-name');
    const isIncluded = Boolean(name.match(new RegExp(searchValue, 'i')));

    if (isIncluded) {
      $(portfolio).removeClass('d-none');

    } else {
      $(portfolio).addClass('d-none');
    }
  });
}

module.exports = {
  init,
  onSearchFormSubmit,
  filterProducts
};
